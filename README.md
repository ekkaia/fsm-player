# FSM Player

Application to create Finite State Machines made in Rust with WebAssembly and Vue 3 TypeScript. Available online at https://ekkaia.gitlab.io/fsm-player/. Part of the Ecore2Rust project https://gitlab.univ-nantes.fr/E187954Y/ecore2rust.

⚠️ At this time, the string matcher only works with a deterministic finite automaton.

## Usage

The latest versions of Node and Rust must be installed.

1. `npm ci`

2. `npm run dev`

3. Local server runs on `localhost:3000`

## Features supported

- Create a Finite State Machine
- Test if a string is recognized by the automaton

## CLI

A command line interface is available in `fsm/src`. To use it, go in the folder and start the program with `cargo run`.
