export interface Node {
    name: string;
    isInitial: boolean;
    isTerminal: boolean;
}

export interface Transition {
    character: string;
    output_node: string;
    input_node: string;
}

export interface DisplayEdges {
	[key: string]: DisplayEdge;
}

export interface DisplayNodes {
	[key: string]: DisplayNode;
}

export interface DisplayEdge {
	character: string;
	source: string;
	target: string;
}

export interface DisplayNode {
	name: string;
	type: string;
	isInitial: boolean;
	isTerminal: boolean;
}

export interface Report {
	text: string;
	isError: boolean;
}

export type FsmMessage =
    | "NameAlreadyUsed"
    | "NodeAdded"
    | "NodeAlreadyExist"
    | "NodeDoesNotExist"
    | "TransitionAdded"
    | "TransitionDoesNotExist"
    | "TransitionAlreadyExist"
    | "StringMatchSuccessful"
    | "StringMatchFailed";

export const messages: Record<FsmMessage, Report> = {
    NameAlreadyUsed: {
        text: "Name already used",
        isError: true,
    },
    NodeAdded: {
        text: "Node added",
        isError: false,
    },
    NodeAlreadyExist: {
        text: "Node already exist",
        isError: true,
    },
    NodeDoesNotExist: {
        text: "Node does not exist",
        isError: true,
    },
    TransitionAdded: {
        text: "Transition added",
        isError: false,
    },
    TransitionDoesNotExist: {
        text: "Transition does not exist",
        isError: true,
    },
    TransitionAlreadyExist: {
        text: "Transition already exist",
        isError: true,
    },
    StringMatchSuccessful: {
        text: "The automaton accepts the string",
        isError: false,
    },
    StringMatchFailed: {
        text: "The automaton does not accept the string",
        isError: true,
    },
};