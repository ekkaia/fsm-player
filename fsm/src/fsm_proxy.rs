use serde::{Serialize, Deserialize};
use wasm_bindgen::prelude::*;
use std::{cell::RefCell, rc::Rc};

use crate::fsm::{FiniteStateMachine, Node, Transition};

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Message {
	StringMatchSuccessful = "StringMatchSuccessful",
	StringMatchFailed = "StringMatchFailed",
	NameAlreadyUsed = "NameAlreadyUsed",
	NodeAdded = "NodeAdded",
	NodeAlreadyExist = "NodeAlreadyExist",
	NodeDoesNotExist = "NodeDoesNotExist",
	TransitionAdded = "TransitionAdded",
	TransitionDoesNotExist = "TransitionDoesNotExist",
	TransitionAlreadyExist = "TransitionAlreadyExist",
}

#[wasm_bindgen]
pub struct FiniteStateMachineProxy {
	finite_state_machine: FiniteStateMachine,
}

#[wasm_bindgen]
impl FiniteStateMachineProxy {
	#[wasm_bindgen(constructor)]
	pub fn new() -> FiniteStateMachineProxy {
		FiniteStateMachineProxy {
			finite_state_machine: FiniteStateMachine {
				nodes: Vec::new(),
				transitions: Vec::new(),
			}
		}
	}

	pub fn add_node(&mut self, name: String, is_initial: bool, is_terminal: bool) -> Message {
		let is_name_already_used: Option<Rc<RefCell<Node>>> = self.find_node_by_name(name.clone());
		match is_name_already_used {
			None => {
				self.finite_state_machine.add_node(name, is_initial, is_terminal);
				return Message::NodeAdded;
			},
			Some(_) => Message::NameAlreadyUsed,
		}
	}

	pub fn add_transition(&mut self, character: char, input_node_name: String, output_node_name: String) -> Message {
		let opt_rc_input_node: Option<Rc<RefCell<Node>>> = self.find_node_by_name(input_node_name);
		let opt_rc_ouput_node: Option<Rc<RefCell<Node>>> = self.find_node_by_name(output_node_name);
		if opt_rc_input_node.is_none() || opt_rc_ouput_node.is_none() {
			return Message::NodeDoesNotExist;
		}

		let rc_input_node: Rc<RefCell<Node>> = opt_rc_input_node.unwrap();
		let rc_output_node: Rc<RefCell<Node>> = opt_rc_ouput_node.unwrap();

		let is_transition_already_exists: Option<Rc<RefCell<Transition>>> = self.find_transition_by_input_output(Rc::clone(&rc_input_node), Rc::clone(&rc_output_node), character);

		match is_transition_already_exists {
			None => {
				self.finite_state_machine.add_transition(character, &rc_input_node, &rc_output_node);
				return Message::TransitionAdded;
			},
			Some(_) => Message::TransitionAlreadyExist,
		}
	}

	pub fn string_matching(&self, word: String) -> Message {
		let is_success = self.finite_state_machine.string_matching(word);
		match is_success {
			true => Message::StringMatchSuccessful,
			false => Message::StringMatchFailed,
		}
	}

	fn find_node_by_name(&mut self, node_name: String) -> Option<Rc<RefCell<Node>>> {
		let mut rc_node: Option<Rc<RefCell<Node>>> = None;
		for node in self.finite_state_machine.nodes.iter() {
			if node_name == node.borrow().name {
				rc_node = Some(Rc::clone(&node));
			}
		}
		return rc_node;
	}

	fn find_transition_by_input_output(&mut self, input_node: Rc<RefCell<Node>>, output_node: Rc<RefCell<Node>>, character: char) -> Option<Rc<RefCell<Transition>>> {
		let mut rc_transition: Option<Rc<RefCell<Transition>>> = None;
		for transition in self.finite_state_machine.transitions.iter() {
			if transition.borrow().input_node.borrow().name == input_node.borrow().name
			&& transition.borrow().output_node.borrow().name == output_node.borrow().name
			&& transition.borrow().character == character {
				rc_transition = Some(Rc::clone(&transition));
			}
		}
		return rc_transition;
	}

	#[wasm_bindgen(method, getter)]
	pub fn nodes(&self) -> JsValue {
		let mut nodes: Vec<LightNode> = Vec::new();
		for node in self.finite_state_machine.nodes.iter() {
			nodes.push(LightNode {
				name: node.borrow().name.clone(),
				is_initial: node.borrow().is_initial,
				is_terminal: node.borrow().is_terminal,
			});
		}
		return JsValue::from_serde(&nodes).unwrap();
	}

	#[wasm_bindgen(method, getter)]
	pub fn transitions(&self) -> JsValue {
		let mut transitions: Vec<LightTransition> = Vec::new();
		for transition in self.finite_state_machine.transitions.iter() {
			transitions.push(LightTransition {
				character: transition.borrow().character,
				input_node: LightNode {
					name: transition.borrow().input_node.borrow().name.clone(),
					is_initial: transition.borrow().input_node.borrow().is_initial,
					is_terminal: transition.borrow().input_node.borrow().is_terminal,
				},
				output_node: LightNode {
					name: transition.borrow().output_node.borrow().name.clone(),
					is_initial: transition.borrow().output_node.borrow().is_initial,
					is_terminal: transition.borrow().output_node.borrow().is_terminal,
				},
			});
		}
		return JsValue::from_serde(&transitions).unwrap();
	}

	pub fn display(&self) -> String {
        return self.finite_state_machine.display();
    }
}

#[derive(Serialize, Deserialize)]
pub struct LightNode {
	pub name: String,
	pub is_initial: bool,
	pub is_terminal: bool,
}

#[derive(Serialize, Deserialize)]
pub struct LightTransition {
	pub input_node: LightNode,
	pub output_node: LightNode,
	pub character: char,
}
