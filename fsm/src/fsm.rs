use std::{cell::RefCell, rc::Rc};

#[derive(Debug, Clone)]
pub struct FiniteStateMachine {
	pub nodes: Vec<Rc<RefCell<Node>>>,
	pub transitions: Vec<Rc<RefCell<Transition>>>,
}

#[derive(Debug, Clone)]
pub struct Node {
	pub name: String,
	pub is_initial: bool,
	pub is_terminal: bool,
	pub input_transitions: Vec<Rc<RefCell<Transition>>>,
	pub output_transitions: Vec<Rc<RefCell<Transition>>>,
}

#[derive(Debug, Clone)]
pub struct Transition {
	pub character: char,
	pub output_node: Rc<RefCell<Node>>,
	pub input_node: Rc<RefCell<Node>>,
}

impl FiniteStateMachine {
	pub fn new() -> Self {
		FiniteStateMachine {
			nodes: Vec::new(),
			transitions: Vec::new(),
		}
	}

	pub fn add_node(
		&mut self,
		name: String,
		is_initial: bool,
		is_terminal: bool,
	) -> Rc<RefCell<Node>> {
		let node = Node {
			name,
			input_transitions: Vec::new(),
			output_transitions: Vec::new(),
			is_initial,
			is_terminal,
		};
		let rc_node = Rc::new(RefCell::new(node));
		self.nodes.push(Rc::clone(&rc_node));
		return Rc::clone(&rc_node);
	}

	pub fn add_transition(
		&mut self,
		character: char,
		input_node: &Rc<RefCell<Node>>,
		output_node: &Rc<RefCell<Node>>,
	) -> Rc<RefCell<Transition>> {
		let transition = Transition {
			character,
			input_node: input_node.clone(),
			output_node: output_node.clone(),
		};
		let rc_transition = Rc::new(RefCell::new(transition));
		self.transitions.push(Rc::clone(&rc_transition));
		input_node
			.borrow_mut()
			.output_transitions
			.push(Rc::clone(&rc_transition));
		output_node
			.borrow_mut()
			.input_transitions
			.push(Rc::clone(&rc_transition));
		return Rc::clone(&rc_transition);
	}

	pub fn display(&self) -> String {
		let mut string_representation: String = String::new();
		for transition in &self.transitions {
			string_representation.push_str(&format!(
				"({}) -{}-> ({})\n",
				transition.borrow().input_node.borrow().name,
				transition.borrow().character,
				transition.borrow().output_node.borrow().name
			));
		}
		return string_representation;
	}

	pub fn string_matching(&self, word: String) -> bool {
		let mut current_node = self
			.nodes
			.iter()
			.find(|node| node.borrow().is_initial)
			.and_then(|node| Some(node.clone()));

		for character in word.chars() {
			current_node = current_node
				.unwrap()
				.borrow()
				.output_transitions
				.iter()
				.find(|transition| transition.borrow().character == character)
				.and_then(|transition| Some(transition.borrow().output_node.clone()));

			if current_node.is_none() {
				return false;
			}
		}
		if !current_node.unwrap().borrow().is_terminal {
			return false;
		}
		return true;
	}
}
