use fsm::FiniteStateMachine;
use fsm_proxy::FiniteStateMachineProxy;
use colored::*;
use text_io::scan;

mod fsm;
mod fsm_proxy;

pub fn test() {
    let mut fsm = FiniteStateMachine::new();
    let q0 = fsm.add_node(String::from("q0"), true, false);
    let q1 = fsm.add_node(String::from("q1"), false, false);
    let q2 = fsm.add_node(String::from("q2"), false, true);
    fsm.add_transition('a', &q0, &q1);
    fsm.add_transition('b', &q1, &q2);

    println!("{}", fsm.display());
    println!("{}", fsm.string_matching(String::from("ab")));
}

pub fn test_proxy() {
    let mut proxy = FiniteStateMachineProxy::new();
    proxy.add_node(String::from("q0"), true, false);
    proxy.add_node(String::from("q1"), false, false);
    proxy.add_node(String::from("q2"), false, true);
    proxy.add_transition('a', String::from("q0"), String::from("q1"));
    proxy.add_transition('b', String::from("q1"), String::from("q2"));

    println!("{:?}", proxy.string_matching(String::from("ab")));
}


pub fn cli() {
    println!("{}", "Welcome to Finite State Machine CLI.".bold().blue());
    let finite_state_machine = FiniteStateMachineProxy::new();
    commands(finite_state_machine);
}

pub fn commands(mut finite_state_machine: FiniteStateMachineProxy) {
    println!("{}", "\nUse the following commands to build your Finite State Machine:".bold().blue());
    println!("{}", "  [1] add a node\t[2] add a transition\t[3] string matcher".blue());
    println!("{}", "  [4] display petrinet\t[5] exit".blue());
    let command: usize;
    scan!("{}", command);
    match command {
        1 => {
            let name: String;
            let is_initial: bool;
            let is_final: bool;
            println!("{}", "\nEnter information about the node: <name> <is_initial> <is_final>".blue());
            scan!("{} {} {}", name, is_initial, is_final);
            let msg = format!("{:?}", finite_state_machine.add_node(name, is_initial, is_final));
            println!("{}", msg.magenta().italic());
            commands(finite_state_machine);
        }
        2 => {
            let character: char;
            let input_node: String;
            let output_node: String;
            println!("{}", "\nEnter information about the transition: <char> <input_node> <output_node>".blue());
            scan!("{} {} {}", character, input_node, output_node);
            let msg = format!("{:?}", finite_state_machine.add_transition(character, input_node, output_node));
            println!("{}", msg.magenta().italic());
            commands(finite_state_machine);
        }
        3 => {
            let word: String;
            println!("{}", "\nEnter a word to test if the FSM recognizes it: <word>".blue());
            println!("{}", "  Note that string matcher only works on non-deterministic finite automata.".blue());
            scan!("{}", word);
            let msg = format!("{:?}", finite_state_machine.string_matching(word));
            println!("{}", msg.magenta().italic());
            commands(finite_state_machine);
        }
        4 => {
            let msg = format!("{}", finite_state_machine.display());
            println!("{}", msg.magenta().italic());
            commands(finite_state_machine);
        }
        5 => {
            println!("{}", "\nExit...".italic());
        }
        _ => {
            println!("{}", "\ninvalid command!".italic());
        }
    }
}